# Wolkenbauer
Build your stuff in the cloud.

## Goals

Automatically create VPSs in the cloud, enable access to them (SSH) and run provisioning steps.
Finally, print connection details.
Remember the VPSs and automagically delete them after a specified amount of time.
