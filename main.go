package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/hetznercloud/hcloud-go/hcloud"
)

type HetznerConf struct {
	Token string `json:"Token"`
}

type ConfStruct struct {
	Hetzner     HetznerConf `json:"Hetzner"`
	PostInstall []string    `json:"PostInstall"`
}

var Conf ConfStruct

func readConfig(file string) (c ConfStruct) {
	configFile, err := os.Open(file)
	if err != nil {
		log.Printf("error opening config file %s: %s\n", file, err.Error())
		return
	}

	jsonParser := json.NewDecoder(configFile)
	if err = jsonParser.Decode(&c); err != nil {
		log.Printf("error parsing config file %s: %s\n", file, err.Error())
		return
	}

	return c
}

func validateConfig(c ConfStruct) (ok bool) {
	ok = true
	if c.Hetzner.Token == "" {
		log.Println("No Token in configuration found.")
		ok = false
	}

	return ok
}

func main() {

	Conf = readConfig("/home/jack/.config/wolkenbauer/config.json")
	if &Conf == nil {
		panic("Aborting.\n")
	}

	if validateConfig(Conf) == true {
		panic("Aborting.\n")
	}

	client := hcloud.NewClient(hcloud.WithToken(Conf.Hetzner.Token))

	var serverOptions = &hcloud.ServerCreateOpts{
		Name: "test-123",
		ServerType: &hcloud.ServerType{
			Name: "cx11",
		},
		Image: &hcloud.Image{
			Name: "debian-9",
		},
		// SSHKeys: [
		// 	&hcloud.SSHKey{nil},
		// ],
		Location: &hcloud.Location{
			Name: "fsn1", // nbg1
		},
		UserData: "wolkenbauer",
	}

	err := serverOptions.Validate()
	if err != nil {
		log.Println(err.Error())
		panic("Aborting.\n")
	}

	ctx := context.Background()
	server, response, err := client.Server.Create(ctx, *serverOptions)
	if err != nil {
		log.Fatalf("error retrieving server: %s\n", err)
	}

	fmt.Printf("Server: %s - %d\n", server.Server.Name, server.Server.ID)
	fmt.Printf("response proto: %s\n", response.Proto)

}
